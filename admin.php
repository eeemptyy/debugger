<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="A Web application during trainee in OSC Kasetsart University">
    <meta name="author" content="Jompol Sermsook, Thanks to startbootstrap for css framework, Thanks.">
    <title>Debugger_Index</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/landing-page.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <?php
    session_start();

    if (!(isset($_SESSION['login']) && $_SESSION['username'] != '')) {
        header ("Location: Login.html");
        exit();
    } else {
        $login =  $_SESSION['login'];
        $idTeacher =  $_SESSION['username'];
    }
    if ($login != "true") {
        header ("Location: Login.html");
        exit();
    }
    // $login = "EMPTY";
    echo "<p id='user' hidden>$login</p>";
?>
</head>

<body>
    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-fixed-top topnav" role="navigation">
        <div class="container topnav">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a id="header-name" class="navbar-brand topnav" href="#">Start Bootstrap</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="index.html">Debugger</a>
                    </li>
                    <li>
                        <a href="controller/kill_session.php">Logout</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <!-- Page Content -->
    <div class="content-section-b">
        <div align="center">
            <h1>Problem List</h1>
            <div class="row">
                <div class="col-sm-2">
                </div>
                <div class="col-sm-8" id="problem_table_div">
                    <table class="table table-bordered" id="problem_table">
                        <thead>
                            <tr>
                                <th>Problem Name</th>
                                <th>Time limit</th>
                                <th>file</th>
                                <th>Show</th>
                                <th></th>
                            </tr>
                        </thead>
                    </table>
                </div>
                <div class="col-sm-2">
                </div>
            </div>
            <div align="center">
                <form class="form-inline" id="addProblem" action="controller/upload.php" method="post" enctype="multipart/form-data" hidden>
                    <div class="form-group">
                        <input type="text" class="form-control" id="problem_name" name="problem_name" placeholder="Problem name" data-toggle="tooltip" data-placement="top" title="Must same as file name">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" id="time_limit" name="time_limit" placeholder="Time limit" data-toggle="tooltip" data-placement="top" title="an Integer ex. 1,2">
                    </div>
                    <div class="form-group">
                        <input type="file" name="fileToUpload" id="fileToUpload">
                    </div>
                    <input type="submit" value="Save" name="submit" class="btn btn-primary">
                </form>
                <button class="btn btn-primary" type='button' onclick='addProblemForm(this)'>Add problem</button>
            </div>
        </div>
    </div>
    <!-- /.content-section-a -->

    <!-- Footer -->
    <footer>
        <div class="container ">
            <p class="text-muted small " style="text-align:right;opacity: 0.5; ">&nbsp;&nbsp;&nbsp;version 0.1.4&nbsp;&nbsp;&nbsp;<a href="http://guestscounter.com" style="font-size:9px;">free hit counter</a>
                <a href="http://guestscounter.com"><img src="http://guestscounter.com/count.php?c_style=139&id=1484469952" border=0 alt="free hit counter"></a>
            </p>
            <div class="row " align="center ">
                <div class="col-lg-12 ">
                    <p class="copyright text-muted small " style="text-align:center;opacity: 0.5; ">Copyright &copy; 2016 CNC Lab | Communication Networks and Cloud Research Laboratory | Department of Computer Science Kasetsart University
                        <BR> Edited by: <a style="color:black; " title="Mail to editor " href="https://www.facebook.com/messages/jompol.sermsook " target="_blank
                        ">Jompol Sermsook</a> | Theme Designed by: Start Bootstrap Project | Thanks to <a style="color:black; " title="StartBootstrap.com " href="http://startbootstrap.com/ " target="_blank ">Startbootstrap.com</a>, maintained by <a style="color:black; "
                            title="David Miller " href="http://davidmiller.io/ " target="_blank ">David Miller</a> at
                        <a style="color:black; " title="Blackrock Digital " href="http://blackrockdigital.io/ " target="_blank ">Blackrock Digital</a>
                    </p>
                </div>
            </div>
        </div>
    </footer>

    <script src="https://code.jquery.com/jquery-3.1.1.js" integrity="sha256-16cdPddA6VdVInumRGo6IbivbERE8p7CQR3HzTBuELA=" crossorigin="anonymous"></script>
    <script src="js/bootstrap.js"></script>
    <script src="js/admin.js"></script>
</body>

</html>