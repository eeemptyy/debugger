<?php
   require 'db_controller.php';

    /* this file will act as a gate to db_controller on ajax called */ 

    $db_controller = new DB_Controller();

    $case_sw = $_POST['func'];
    switch ($case_sw) {
        case "get_login":
            $username = $_POST['username'];
            echo $db_controller->getLogin($username);
            break;
        case "get_problem_table":
            echo $db_controller->getProblemTable();
            break;
        case "get_problem_data":
            $problem = $_POST['problem'];
            echo $db_controller->getProbData($problem);
            break;
        case "update_problem":
            $problem = $_POST['problem'];
            echo $db_controller->updateProblem($problem);
            break;
        case "get_problem_name":
            echo $db_controller->getProbName();
            break;
        case "delete_problem":
            $problem = $_POST['problem'];
            echo $db_controller->deleteProblem($problem);
            break;
        default:
            echo "Function Not Found.";
}

?>