<?php
    $problem = $_POST['problem'];
    $inputFld = $_POST['inputFld'];
    $timeLimit = $_POST['timeLimit'];
    file_put_contents("inn.in",$inputFld);
    file_put_contents("probname.in",$problem);
    
    if (!file_exists("..\\files\\".$problem.".java")){
        $q = "Solution file doesn't exists. Please contact Admin.";
        echo "\n$q\n$q\n$q\n$q\n";
    }else {
        $output = shell_exec("java -version 2>&1");
        if (!file_exists("..\\files\\".$problem.".class")){
            shell_exec("copy ..\\files\\".$problem.".java ".$problem.".java");
            $output .= shell_exec("javac ".$problem.".java 2>&1");
            sleep(2);
            shell_exec("copy ".$problem.".class ..\\files\\".$problem.".class");
        }else {
            shell_exec("copy ..\\files\\".$problem.".class ".$problem.".class");        
        }
        $fromjava = shell_exec("python pythonContainer.py 2>&1 ".$timeLimit);
        $myfile = fopen("out.ans", "r") or die("Unable to open file!");
        while(!feof($myfile)) {
            $output = $output.fgets($myfile);   
        }
        fclose($myfile);
        echo $output;
        shell_exec("del ".$problem.".*");
        shell_exec("del inn.in out.ans probname.in");
    }
?>