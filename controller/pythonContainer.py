import subprocess, os, signal, sys
import datetime  as time

def getSecMicro(now):
    nn = str(now)
    secs = nn.split(sep=".")[0].split(sep=":")[2][-1]
    micro = nn.split(".")[1][0:4]
    return secs, micro
	
# Declare part
timeout = int(sys.argv[1])
isKill = False
f = open('probname.in')
problemName = f.readline()
myinput = open('inn.in')
myoutput = open('out.ans', 'w+')
start = time.datetime.now()
javaProc = subprocess.Popen(["java", problemName], stdin=myinput, stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)
while javaProc.poll() is None:
    now = time.datetime.now()
    if (now-start).seconds > timeout:
        isKill = True
        myoutput.write("Manual Kill process after {} seconds.\n".format(timeout))
        break
now = time.datetime.now()
if isKill:
    myoutput.write("Time Limit Exceeds\n")
    os.kill(javaProc.pid, signal.CTRL_C_EVENT)
else :
    elpstime = now-start
    secs, micro = getSecMicro(elpstime)
    myoutput.write("run complete with time {}.{} seconds\n".format(secs, micro))
    out = ""
    if javaProc.returncode == 0:
        for i in javaProc.stdout:
            if i == "" or i == " ":
                continue
            else :
                out += i
    else :
        myoutput.write('else')
        for i in javaProc.stderr:
            if i == "" or i == " ":
                continue
            else :
                out += i
    
    myoutput.write(out+"\n")
    myoutput.close()