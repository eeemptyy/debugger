<?php
require 'db_controller.php';
$db_controller = new DB_Controller();

$target_dir = "../files/";
$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
$uploadOk = 1;
$upFileType = pathinfo($target_file,PATHINFO_EXTENSION);
// Check if file empty.
if(isset($_POST["submit"])) {
    $check = filesize($_FILES["fileToUpload"]["tmp_name"]);
    // echo $check;
    if($check !== false) {
        // echo "File is an correct - " . $check["mime"] . ".";
        $uploadOk = 1;
    } else {
        // echo "File is not an image.";
        $uploadOk = 0;
    }
}

$num = array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9');
if (!in_array($_POST['time_limit'], $num)){
    echo "Sorry, Time Limit must be an Integer. <BR>";
    $uploadOk = 0;
}
// Check if problem name and filename not the same
if ($_POST['problem_name'].'.java' != $_FILES["fileToUpload"]["name"]){
    echo "Sorry, Filename and Problemname must be the same. <BR>";
    $uploadOk = 0;
}
// Check if file already exists
if (file_exists($target_file)) {
    echo "Sorry, file already exists. <BR><big>If you want to upload new version of this file please 'Delete' first.<BR></big>";
    $uploadOk = 0;
}
// Check file size
if ($_FILES["fileToUpload"]["size"] > 500000) {
    echo "Sorry, your file is too large.<BR>";
    $uploadOk = 0;
}
// Allow certain file formats // Temp save && $upFileType != "py"
if($upFileType != "java") {
    echo "Sorry, only java files are allowed.<BR>";
    $uploadOk = 0;
}
// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
    echo "Sorry, your file was not uploaded.<BR>";
// if everything is ok, try to upload file
} else {
    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
        $timeLimit = $_POST['time_limit'];
        $problemName = $_POST['problem_name']; 
        $prob = array();
        $prob['name'] = $problemName;
        $prob['time_limit'] = $timeLimit;
        $prob['file'] = $_FILES["fileToUpload"]["name"];
        $json_prob = json_encode($prob);
        $out = $db_controller->addProblem($json_prob);

        echo "$out \File ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";
    } else {
        echo "Sorry, there was an error uploading your file.<BR>";
    }
}
?>