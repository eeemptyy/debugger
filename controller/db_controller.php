<?php

class DB_Controller{
    
    private $username = "";
    private $password = "";
    private $hostname = "";
    private $dbname = "";   

    private $connection;
    
    public function __construct(){
        include("dbconfig.php");
        $this->username = $uname;
        $this->password = $pass;
        $this->hostname = $host;
        $this->dbname = $db;
        
        try{
            
            $this->connection = new PDO("mysql:host={$this->hostname};"."dbname={$this->dbname}",$this->username,$this->password);
            // $this->connection->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
            
        } catch (PDOException $e){
            die("Couldn't connect to the database ".$dbname.": ".$e->getMessage());
        }
    }

    public function getProbName(){
        try {
            $sql = "SELECT name FROM problem";
            $q = $this->connection->query($sql);
            $q->setFetchMode(PDO::FETCH_ASSOC);
        } catch (Exception $e) {
            die($e->getMessage());
        }
        $problems = array();
        $n=1;
        while ($row = $q->fetch()){
            $problems[$n] = $row['name'];
            $n++;
        }
        return json_encode($problems);
    }

    public function getProbData($prob){
        try {
            $sql = 'SELECT * FROM problem WHERE problem.name = "'.$prob.'"';
            $q = $this->connection->query($sql);
            $q->setFetchMode(PDO::FETCH_ASSOC);
        } catch (Exception $e) {
            die($e->getMessage());
        }
        $problems = array();
        while ($row = $q->fetch()){
            $problems['name'] = $row['name'];
            $problems['time_limit'] = $row['time_limit'];
        }
        return json_encode($problems);
    }

    public function getProblemTable(){  
        try {
            $sql = "SELECT * FROM problem";
            $q = $this->connection->query($sql);
            $q->setFetchMode(PDO::FETCH_ASSOC);
        } catch (Exception $e) {
            die($e->getMessage());
        }
        
        $problems = array();
        $n=1;
        while ($row = $q->fetch()){
            $problem = array();
            $problem['name'] = $row['name'];
            $problem['time_limit'] = $row['time_limit'];
            $problem['file'] = $row['file'];
            $temp = $row['show_hide'];
            if ($temp == "0"){
                // $temp = '<select>
                //             <option value="hide">Hide</option>
                //             <option value="show">Show</option>
                //         </select>';
                $temp = '<input checked="false" type="checkbox">Show<br>';
            }else {
                // $temp = '<select>
                //             <option value="show">Show</option>
                //             <option value="hide">Hide</option>
                //         </select>';
                $temp = '<input checked="true" type="checkbox">Show<br>';
            }
            $problem['show_hide'] = $temp;
            $problems[$n] = $problem;
            $n++;
        }
        /*
            Start creating data fot Problem table
        */
        $txt = "<table class='table table-bordered' id='problem_table'>"."<thead><tr>
                    <thead>
                        <tr>
                            <th>Problem Name</th>
                            <th>Time limit</th>
                            <th>file</th>
                            <th>Visible</th>
                            <th></th>
                        </tr>
                    </thead>";
        $txt .= "   <tbody>";
        foreach ($problems as $prob) {
            $txt .= "   <tr><td>".$prob['name']."</td>
                            <td>".$prob['time_limit']."</td>
                            <td>".$prob['file']."</td>
                            <td>".$prob['show_hide']."</td>
                            <td><button type='button'  onclick='editProblem(this)'>Edit</button>
                                <button type='button'  onclick='deleteProblem(this)'>Del</button>
                            </td></tr>";
        }
        $txt .= "   </tbody></table>";
        echo $txt;
    }

    public function updateProblem($prob){
        $problem = json_decode($prob);
        $problem_dict = array();
        foreach ($problem as $key => $val) {
            $problem_dict[$key] = $val;
        }
        $name = $problem_dict['name'];
        $timeLimit = $problem_dict['time_limit'];
        $file = $problem_dict['file'];
        $show_hide = $problem_dict['show_hide'];
        if ($show_hide == "hide"){
            $dat = 0;
        }else {
            $dat = 1;
        }
        try {
            $sql = 'UPDATE problem
                    SET name = "'.$name.'", time_limit = "'.$timeLimit.'", file = "'.$file.'", show_hide = "'.$dat.'" 
                    WHERE name = "'.$name.'"';
            $q = $this->connection->prepare($sql);
            $q->execute();
        } catch (Exception $e) {
            die($e->getMessage());
        }
            // return "Server Response. $idSubject $idStudent $quarter1 $quarter2 $quarter3 $semester $year";
            return "Server Response. $dat ++ $show_hide";
    }

    public function deleteProblem($prob){
        $problem = json_decode($prob);
        $problem_dict = array();
        foreach ($problem as $key => $val) {
            $problem_dict[$key] = $val;
        }
        $name = $problem_dict['name'];
        $timeLimit = $problem_dict['time_limit'];
        $file = $problem_dict['file'];
        try {
            $sql = 'DELETE FROM problem WHERE problem.name = "'.$name.'"';
            $q = $this->connection->prepare($sql);
            $q->execute();
        } catch (Exception $e) {
            die($e->getMessage());
        }
            // return "Server Response. $idSubject $idStudent $quarter1 $quarter2 $quarter3 $semester $year";
            shell_exec("del ..\\files\\".$name.".*");
            return "Server Response, Delete success";
    }

    public function addProblem($prob){
        $problem = json_decode($prob);
        $problem_dict = array();
        foreach ($problem as $key => $val) {
            $problem_dict[$key] = $val;
        }
        $name = $problem_dict['name'];
        $timeLimit = $problem_dict['time_limit'];
        $file = $problem_dict['file'];
        try {
            $sql = 'INSERT INTO problem (name, time_limit, file) VALUES ("'.$name.'", "'.$timeLimit.'", "'.$file.'")';
            $q = $this->connection->prepare($sql);
            $q->execute();
        } catch (Exception $e) {
            die($e->getMessage());
        }
        return "File $name has been insert into database successful with TimeLimit=$timeLimit Filename=$file";
            // return "Server Response.".$name." ".$timeLimit." ".$file;
    }

    public function getLogin($username)
    {
        // return $username." Res";
        try {
            $sql = "SELECT * FROM admin WHERE username = '".$username."'";
            $q = $this->connection->query($sql);
            $q->setFetchMode(PDO::FETCH_ASSOC);
        } catch (Exception $e) {
            die($e->getMessage());
        }        
        // return "Serv A";
        
        $admin=array();
        while ($row = $q->fetch()) {
            $admin['username'] = $row['username'];
            $admin['password'] = $row['password'];
        }
        return json_encode($admin);
    }
}
?>