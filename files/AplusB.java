import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class AplusB {
	private static BufferedReader bf;

	public static void main(String[] args) throws IOException {
		bf = new BufferedReader(new InputStreamReader(System.in));
		int a = Integer.parseInt(bf.readLine());
		int b = Integer.parseInt(bf.readLine());
		int c = a+b;
		System.out.println(c);
    }
}