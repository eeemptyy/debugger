$(document).ready(function() {
    $('#header-name').html('Hi ' + $('#user').html() + '!');
    $('[data-toggle="tooltip"]').tooltip();
    getTable();
});

function getTable() {
    $.ajax({
        type: "POST",
        url: "controller/phpfunction_switcher.php",
        data: {
            func: 'get_problem_table',
        },
        success: function(data) {
            // alert("update success " + data);
            $('#problem_table_div').html(data);
        },
        error: function(data) {
            alert("error " + data);
        }
    });
}

function deleteProblem(button) {
    if (confirm('Are you sure you want to delete this problem from the database?')) {
        alert("Yes");
        var currentTD = $(button).parents('tr').find('td');
        var outData = {};
        var name = currentTD.eq(0).html();
        var time_limit = currentTD.eq(1).html();
        var is_compile = currentTD.eq(2).html();
        var file = currentTD.eq(2).html();
        outData['name'] = name
        outData['time_limit'] = time_limit
        outData['file'] = file;
        var json_problem = JSON.stringify(outData);

        $.ajax({
            type: "POST",
            url: "controller/phpfunction_switcher.php",
            data: {
                func: 'delete_problem',
                problem: json_problem
            },
            success: function(data) {
                // alert("update success " + data);
                location.reload();

            },
            error: function(data) {
                alert("error " + data);
            }
        });
    }
}

function editProblem(button) {
    var currentTD = $(button).parents('tr').find('td');
    if ($(button).html() == "Edit") {
        $(button).html("Save");
        for (i = 1; i < 4; i += 2) {
            $(currentTD.eq(i)).prop('contenteditable', true);
            $(currentTD.eq(i)).css({ "background-color": "#ff9999" });
        }
    } else {
        var outData = {};
        var name = currentTD.eq(0).html();
        var time_limit = currentTD.eq(1).html();
        var file = currentTD.eq(2).html();
        var show_hide = currentTD.eq(3).prop("checked");
        outData['name'] = name
        outData['time_limit'] = time_limit
        outData['file'] = file;
        alert(show_hide + " >>><<<>");
        outData['show_hide'] = show_hide;
        var json_problem = JSON.stringify(outData);
        if (!isEmpty(name) && !isEmpty(time_limit) && !isEmpty(file)) {
            $(button).html("Edit");
            for (i = 1; i < 4; i += 2) {
                $(currentTD.eq(i)).prop('contenteditable', false);
                $(currentTD.eq(i)).css({ "background-color": "#FFFFFF" });
            }
            $.ajax({
                type: "POST",
                url: "controller/phpfunction_switcher.php",
                data: {
                    func: 'update_problem',
                    problem: json_problem
                },
                success: function(data) {
                    alert(data);
                    location.reload();
                    // alert("update success " + data);
                },
                error: function(data) {
                    location.reload();
                    alert("error " + data);
                }
            });
        } else {
            alert("invalid data");
        }
    }
}

function addProblemForm(button) {
    var temp = $(button).html();
    if (temp == "Add problem") {
        $('#addProblem').show();
        $(button).html("Cancle");
    } else {
        $('#addProblem').hide();
        $(button).html("Add problem");
    }
}

function isEmpty(data) {
    if (data.trim() == "") {
        return true;
    }
    return false;
}

$('#addProblem').submit(function() {
    window.open('', 'formpopup', 'width=400,height=400,resizeable,scrollbars');
    this.target = 'formpopup';
});