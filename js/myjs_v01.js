var globOut = "";

$(document).ready(function() {
    $('#outputFld2').attr("readonly", true);
    getProblemName();
});

function runcodeOnIndex01() {
    $('#myPleaseWait').modal('show');
    $.ajax({
        type: "POST",
        url: "controller/phpfunction_switcher.php",
        data: {
            problem: $('#problem').val(),
            func: 'get_problem_data',
        },
        success: function(data) {
            var obj = JSON.parse(data);
            if (obj == "") {
                $('#outputFld2').val("Can't find problem solutions \n" + data);
                $('#myPleaseWait').modal('hide');
            } else {
                var probname = obj['name'];
                var time = obj['time_limit'];
                runcodeOnIndex02(probname, time);
            }
        },
        error: function(data) {
            $('#myPleaseWait').modal('hide');
        }
    });
}

function runcodeOnIndex02(probname, time) {
    $('#myPleaseWait').modal('show');
    $.ajax({
        type: "POST",
        url: "controller/Runcode_on_index.php",
        data: {
            problem: probname,
            inputFld: $('#inputFld').val(),
            timeLimit: time
        },
        success: function(data) {
            globOut = data;
            $('#outputFld2').val(data);
            $('#myPleaseWait').modal('hide');
        },
        error: function(data) {
            $('#outputFld2').val(data);
            $('#myPleaseWait').modal('hide');
        }
    });
}

function getProblemName() {
    $.ajax({
        type: "POST",
        url: "controller/phpfunction_switcher.php",
        data: {
            func: 'get_problem_name',
        },
        success: function(data) {
            genDrop(data);
        },
        error: function(data) {
            alert("error " + data);
        }
    });
}

function genDrop(data) {
    var obj = JSON.parse(data);
    var length = 0;
    for (var k in obj) {
        if (obj.hasOwnProperty(k)) length++;
    }
    var txt = 'Problem Name: <select name="model_drop" id="problem">';
    for (i = 1; i < length + 1; i++) {
        txt += "<option>" + obj[i] + "</option>";
    }
    txt += "</select>";
    $('#problem').html(txt);
}